<?php get_header(); ?>

    <!-- Banner -->
    <section id="banner">
        <h2><?php bloginfo('name'); ?></h2>
        <p><?php bloginfo('description'); ?></p>
    </section>

    <!-- Main -->
    <section id="main" class="container">
        <?php
        //$landing_query = new WP_Query(['meta_query' => [['key' => 'is_landing', 'value' => '1', 'compare' => '==']]]);
        $landing_query = new WP_Query('post_type=page');
        if ($landing_query->have_posts()) : ?>
        <?php while($landing_query->have_posts()) : $landing_query->the_post(); ?>
        <?php if (get_field('is_landing')) :?>
        <section class="box special">
            <header class="major">
                <h2><?php the_title_attribute();?></h2>
                <p><?php the_field('subheader');?></p>
            </header>
            <?php
            if (has_post_thumbnail()) : ?>
                <span class="image featured">
                    <?php the_post_thumbnail('full'); ?>
                </span>
            <?php endif; ?>
        </section>
        <?php endif; ?>
        <?php endwhile; wp_reset_postdata(); ?>
        <?php endif; ?>
        <?php if (have_posts()) : global $wp_query; ?>
            <?php while (have_posts()): the_post(); ?>
                <?php if (($wp_query->current_post % 2) == 0): ?>
                    <div class="row">
                <?php endif; ?>
                <div class="6u 12u(narrower)">
                    <section <?php post_class("box special"); ?> id="post-<?php the_ID(); ?>">
                                    <span class="image featured">
                                        <?php the_post_thumbnail('medium'); ?>
                                    </span>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt() ?></p>
                        <ul class="actions">
                            <li>
                                <a href="<?= get_the_permalink() ?>" class="button alt">Learn more</a>
                            </li>
                        </ul>
                    </section>
                </div>
                <?php if ((($wp_query->current_post % 2) == 1) || (($wp_query->current_post + 1) == $wp_query->post_count)) : ?>
                    </div> <!-- closing row -->
                <?php endif; ?>
            <?php endwhile; /* конец while */ ?>
            <div class="navigation">
                <div class="next-posts"><?php next_posts_link(); ?></div>
                <div class="prev-posts"><?php previous_posts_link(); ?></div>
            </div>
        <?php else:
            echo "<h2>Записей нет.</h2>";
        endif; ?>
    </section>

    <!-- CTA -->
    <section id="cta">

        <h2>Sign up for beta access</h2>
        <p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc.</p>

        <form>
            <div class="row uniform 50%">
                <div class="8u 12u(mobilep)">
                    <input type="email" name="email" id="email" placeholder="Email Address"/>
                </div>
                <div class="4u 12u(mobilep)">
                    <input type="submit" value="Sign Up" class="fit"/>
                </div>
            </div>
        </form>

    </section>

<?php get_footer(); ?>