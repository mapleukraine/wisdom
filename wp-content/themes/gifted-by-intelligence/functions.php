<?php
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );

add_theme_support( 'menus' );

// Add specific CSS class by filter.
add_filter( 'body_class', function( $classes ) {
    $classesToAdd = [];
    if ( is_front_page() ) {
        $classesToAdd[] = 'landing';
    }
    return array_merge( $classes, $classesToAdd );
} );

function wisdom_header_class()
{
    $classesToAdd = [];
    if ( is_front_page() ) {
        $classesToAdd[] = 'alt';
    }
    return implode(' ', $classesToAdd);
}

