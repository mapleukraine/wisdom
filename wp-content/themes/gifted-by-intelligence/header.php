<!DOCTYPE HTML>
<!--
    Alpha by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Alpha by HTML5 UP</title>
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/main.css"/>
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="<?= wisdom_header_class(); ?>">
        <h1><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></h1>
        <nav id="nav">
            <ul>
                <li><a href="<?php bloginfo('url'); ?>">На главную</a></li>
                <?php wp_nav_menu(array(
                    'theme_location' => '',
                    'menu' => 'Main menu',
                    'container' => '',
                    'menu_class' => 'menu header',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'items_wrap' => '%3$s',
                ));
                ?>
                <li><a href="#" class="button">Sign Up</a></li>
            </ul>
        </nav>
    </header>
